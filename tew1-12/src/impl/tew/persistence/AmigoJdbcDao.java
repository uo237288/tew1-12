package impl.tew.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.tew.model.Usuario;
import com.tew.persistence.AmigoDao;
import com.tew.persistence.exception.PersistenceException;

public class AmigoJdbcDao implements AmigoDao {

	@Override
	public void creaInvi(String emailU, String emailA) {

		PreparedStatement ps = null;
		Connection con = null;

		try {

			// Variables necesarias para usar la BD:
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/localDB";
			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "sa", "");

			String msg = "insert into Amigos (email_usuario, email_amigo, aceptada) "
					+ "values (?, ?, ?)";

			ps = con.prepareStatement(msg);
			ps.setString(1, emailU);
			ps.setString(2, emailA);
			ps.setBoolean(3, false);

			System.out.println(ps.toString());
			ps.executeUpdate();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new PersistenceException("Driver not found", e);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		}
	}

	@Override
	public List<Usuario> InvisRecibidas(String email_u) {

		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		List<Usuario> InvisNoAceptadas = new ArrayList<Usuario>();

		try {
			// En una implemenntaci��n m��s sofisticada estas constantes habr��a
			// que sacarlas a un sistema de configuraci��n:
			// xml, properties, descriptores de despliege, etc
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/localDB";

			// Obtenemos la conexi��n a la base de datos.
			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "sa", "");
			ps = con.prepareStatement("select * from Amigos where email_usuario = ?");

			ps.setString(1, email_u);
			rs = ps.executeQuery();

			while (rs.next()) {
				UsuarioJdbcDao udao = new UsuarioJdbcDao();
				Usuario user = udao.findById(rs.getString("EMAIL_AMIGO"));
				if (!rs.getBoolean("ACEPTADA")) {
					InvisNoAceptadas.add(user);
				}

			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new PersistenceException("Driver not found", e);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception ex) {
				}
			}
			;
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			;
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
				}
			}
			;
		}

		return InvisNoAceptadas;
	}
	
	public List<Usuario> AmigosUsuario(String email_u) {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;

		List<Usuario> Amigos = new ArrayList<Usuario>();

		try {
			// En una implemenntaci��n m��s sofisticada estas constantes habr��a
			// que sacarlas a un sistema de configuraci��n:
			// xml, properties, descriptores de despliege, etc
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/localDB";

			// Obtenemos la conexi��n a la base de datos.
			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "sa", "");
			ps = con.prepareStatement("select * from Amigos where email_usuario = ?");

			ps.setString(1, email_u);
			rs = ps.executeQuery();

			while (rs.next()) {
				UsuarioJdbcDao udao = new UsuarioJdbcDao();
				Usuario user = udao.findById(rs.getString("EMAIL_AMIGO"));
				Amigos.add(user);

			}

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new PersistenceException("Driver not found", e);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception ex) {
				}
			}
			;
			if (ps != null) {
				try {
					ps.close();
				} catch (Exception ex) {
				}
			}
			;
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
				}
			}
			;
		}

		return Amigos;
	}

	@Override
	public void aceptaInvi(String emailU, String emailA) {
		
		PreparedStatement ps = null;
		Connection con = null;

		try {

			// Variables necesarias para usar la BD:
			String SQL_DRV = "org.hsqldb.jdbcDriver";
			String SQL_URL = "jdbc:hsqldb:hsql://localhost/localDB";
			Class.forName(SQL_DRV);
			con = DriverManager.getConnection(SQL_URL, "sa", "");

			String msg = "update Amigos set aceptada = ? where email_usuario = ? and email_amigo = ?";

			ps = con.prepareStatement(msg);
			ps.setBoolean(1, true);
			ps.setString(2, emailU);
			ps.setString(3, emailA);
			ps.executeUpdate();
			
			String msg2 = "insert into Amigos (email_usuario, email_amigo, aceptada) "
					+ "values (?, ?, ?)";
			
			PreparedStatement ps2 = con.prepareStatement(msg2);
			ps2.setString(1, emailA);
			ps2.setString(2, emailU);
			ps2.setBoolean(3, true);
			ps2.executeUpdate();
			

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new PersistenceException("Driver not found", e);
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException("Invalid SQL or database schema", e);
		}
		
	}
}
