package impl.tew.persistence;


import com.tew.persistence.AmigoDao;
import com.tew.persistence.PublicacionDao;
import com.tew.persistence.UsuarioDao;
import com.tew.persistence.PersistenceFactory;


public class SimplePersistenceFactory implements PersistenceFactory {

	@Override
	public UsuarioDao createUsuarioDao() {
		return new UsuarioJdbcDao();
	}

	@Override
	public PublicacionDao createPublicacionDao() {
		return new PublicacionJdbcDao();
	}

	@Override
	public AmigoDao createAmigoDao() {
		return new AmigoJdbcDao();
	}

}
