package impl.tew.business;


import com.tew.business.AmigoService;
import com.tew.business.LoginService;
import com.tew.business.PublicacionService;
import com.tew.business.UsuariosService;
import com.tew.business.ServicesFactory;

public class SimpleServicesFactory implements ServicesFactory {

	@Override
	public UsuariosService createUsuariosService() {
		return new SimpleUsuariosService();
	}

	@Override
	public LoginService createLoginService() {
		return new SimpleLoginService();
	}

	@Override
	public PublicacionService createPublicacionService() {
		return new SimplePublicacionService();
	}

	@Override
	public AmigoService createAmigoService() {
		return new SimpleAmigoService();
	}
}
