package impl.tew.business;

import impl.tew.business.classes.*;

import java.util.List;

import com.tew.business.UsuariosService;
import com.tew.business.exception.EntityAlreadyExistsException;
import com.tew.business.exception.EntityNotFoundException;
import com.tew.model.Usuario;

public class SimpleUsuariosService implements UsuariosService {

	@Override
	public List<Usuario> getUsuarios() throws Exception{
		return new UsuariosListado().getUsuarios();
	}

	@Override
	public void saveUsuario(Usuario Usuario) throws EntityAlreadyExistsException {
		new UsuariosAlta().save(Usuario);
	}

	@Override
	public void deleteUsuario(String id) throws EntityNotFoundException {
		new UsuariosBaja().delete(id);
	}

	@Override
	public Usuario findById(String id) throws EntityNotFoundException {
		return new UsuariosBuscar().find(id);
	}
	
	@Override
	public void reiniciarBD() {
		new UsuariosReiniciar().reiniciarBD();
	}

	@Override
	public void editNombreUsuario(String email, String nombre)
			throws EntityNotFoundException {
		new UsuariosUpdate().editNombre(email, nombre);
		
	}

	@Override
	public void editRolUsuario(String email, String rol)
			throws EntityNotFoundException {
		new UsuariosUpdate().editRol(email, rol);
		
	}
	
	
}
