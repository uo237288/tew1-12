package impl.tew.business;

import java.util.List;

import com.tew.business.PublicacionService;
import com.tew.model.Publicacion;

import impl.tew.business.classes.PublicacionCrearPublicacion;
import impl.tew.business.classes.PublicacionListadoAmigos;
import impl.tew.business.classes.PublicacionListadoMias;

public class SimplePublicacionService implements PublicacionService {

	@Override
	public void crearPublicacion(String titulo, String texto,String email) {
		new PublicacionCrearPublicacion().crearPublicacion(titulo, texto,email);
		
	}

	@Override
	public List<Publicacion> getPublicaciones() throws Exception {
		return new PublicacionCrearPublicacion().getPublicaciones();
	}
	
	@Override
	public List<Publicacion> getPublicacionesAmigos() throws Exception {
		return new PublicacionListadoAmigos().getPublicacionesAmigos();
	}

	@Override
	public List<Publicacion> getMisPublicaciones() throws Exception {
		return new PublicacionListadoMias().getMisPublicaciones();
	}
}
