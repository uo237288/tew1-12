package impl.tew.business.classes;
import com.tew.infrastructure.Factories;
import com.tew.persistence.UsuarioDao;

public class UsuariosReiniciar {

	public void reiniciarBD(){
		UsuarioDao dao = Factories.persistence.createUsuarioDao();
		dao.reiniciarBD();
		
	}
}
