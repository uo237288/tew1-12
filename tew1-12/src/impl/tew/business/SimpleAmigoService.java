package impl.tew.business;

import impl.tew.business.classes.AmigoInvitacion;

import java.util.List;

import com.tew.business.AmigoService;
import com.tew.model.Usuario;

public class SimpleAmigoService implements AmigoService {

	@Override
	public void creaInvi(String emailU, String emailA) {
		new AmigoInvitacion().crearInvitacion(emailU, emailA);
		
	}

	@Override
	public List<Usuario> InvisRecibidas(String emailU) {
		return new AmigoInvitacion().InvisRecibidas(emailU);
	}
	
	@Override
	public List<Usuario> AmigosUsuario(String emailU) {
		return new AmigoInvitacion().AmigosUsuario(emailU);
	}

	@Override
	public void aceptaInvi(String emailU, String emailA) {
		new AmigoInvitacion().aceptaInvitacion(emailU, emailA);
		
	}

}
