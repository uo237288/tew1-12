package com.tew.presentation;

import java.io.Serializable;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.*;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

@ManagedBean
@SessionScoped
public class BeanSettings implements Serializable {
	private static final long serialVersionUID = 2L;
	private static final Locale ENGLISH = new Locale("en");
	private static final Locale SPANISH = new Locale("es");
	private Locale locale = new Locale("es");

	@ManagedProperty(value = "#{Usuario}")
	private BeanUsuario Usuario;

	public BeanUsuario getUsuario() {
		return Usuario;
	}

	public void setUsuario(BeanUsuario Usuario) {
		this.Usuario = Usuario;
	}

	// Se inicia correctamente el Managed Bean inyectado si JSF lo hubiera
	// creado
	// y en caso contrario se crea.
	// (hay que tener en cuenta que es un Bean de sesión)
	// Se usa @PostConstruct, ya que en el contructor no se sabe todavía si
	// el MBean ya estaba construido y en @PostConstruct SI.
	@PostConstruct
	public void init() {
		System.out.println("BeanSettings - PostConstruct");
		// Buscamos el Usuario en la sesión. Esto es un patrón factoría
		// claramente.
		Usuario = (BeanUsuario) FacesContext.getCurrentInstance()
				.getExternalContext().getSessionMap()
				.get(new String("Usuario"));
		// si no existe lo creamos e inicializamos
		if (Usuario == null) {
			System.out.println("BeanSettings - No existia");
			Usuario = new BeanUsuario();
			FacesContext.getCurrentInstance().getExternalContext()
					.getSessionMap().put("Usuario", Usuario);
		}
	}

	// Es sólo a modo de traza.
	@PreDestroy
	public void end() {
		System.out.println("BeanSettings - PreDestroy");
	}

	public Locale getLocale() { /*
								 * Habria que cambiar algo de código para coger
								 * locale del navegador la primera vez que se
								 * accede a getLocale(), de momento el idioma de
								 * partida “es”
								 */
		return (locale);
	}

	public void setSpanish(ActionEvent event) {
		locale = SPANISH;
		try {
			FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
			if (Usuario != null)
				if (Usuario.getEmail() == null) // valores por defecto del
												// Usuario, si no NO inicializar
					Usuario.iniciaUsuario(null);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void setEnglish(ActionEvent event) {
		locale = ENGLISH;
		try {
			FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
			if (Usuario != null)
				if (Usuario.getEmail() == null) // valores por defecto del
												// Usuario, si no NO inicializar
					Usuario.iniciaUsuario(null);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}