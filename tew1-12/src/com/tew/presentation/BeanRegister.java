package com.tew.presentation;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

import com.tew.business.UsuariosService;
import com.tew.infrastructure.Factories;
import com.tew.model.Usuario;

@ManagedBean(name="register")
@RequestScoped
public class BeanRegister implements Serializable {
	
	private static final long serialVersionUID = 6894L;
	private String email = "";
	private String passwd = "";
	private String passwd2 = "";
	private String nombre = "";
	private boolean lopd;
	
	@ManagedProperty(value="#{BeanLogin}") 
    private BeanLogin Login;
	
	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPasswd() {
		return passwd;
	}


	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}


	public String getPasswd2() {
		return passwd2;
	}


	public void setPasswd2(String passwd2) {
		this.passwd2 = passwd2;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	

	public String salva() {
		UsuariosService service;
		try {
			// Acceso a la implementacion de la capa de negocio
			// a trav��s de la factor��a
			FacesContext jsfCtx = FacesContext.getCurrentInstance();
			service = Factories.services.createUsuariosService();
			ResourceBundle bundle = jsfCtx.getApplication().getResourceBundle(
					jsfCtx, "msgs");
			FacesMessage msg = null;
			List<Usuario> usuarios = service.getUsuarios();
			// Comprobamos que el email no es repetido
			Iterator<Usuario> itr = usuarios.iterator();
			while (itr.hasNext()) {
				if (itr.next().getEmail().equals(this.email)) {
					break;
				}
			}
			if (itr.hasNext()) {
				msg = new FacesMessage(FacesMessage.SEVERITY_WARN,
						bundle.getString("alta_form_email_error"), null);
				// se añade al element con id=”msg”
				FacesContext.getCurrentInstance().addMessage(null, msg);
				return "existe";
			}

			else {
				//Comparamos que coinciden los dos campos de contraseña
				if (passwd.equals(passwd2) && this.lopd) {
					Usuario u = new Usuario(this.email, this.passwd, "user", this.nombre);
					service.saveUsuario(u);
					this.Login = new BeanLogin();
					this.Login.putUserInSession(u);
					return "exito";
					
				}
				
				else {
					if (!passwd.equals(passwd2)){
						msg = new FacesMessage(FacesMessage.SEVERITY_WARN,
								bundle.getString("alta_form_contra_error"), null);
						// se añade al element con id=”msg”
						FacesContext.getCurrentInstance().addMessage(null, msg);
						return "existe";
					}
					
					else {
						msg = new FacesMessage(FacesMessage.SEVERITY_WARN,
								bundle.getString("alta_form_lopd_error"), null);
						// se añade al element con id=”msg”
						FacesContext.getCurrentInstance().addMessage(null, msg);
						return "existe";
					}
					
				}
				
			}
			

		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

	}


	public boolean isLopd() {
		return lopd;
	}


	public void setLopd(boolean lopd) {
		this.lopd = lopd;
	}

}
