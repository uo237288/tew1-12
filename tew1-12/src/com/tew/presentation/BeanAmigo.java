package com.tew.presentation;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.tew.business.AmigoService;
import com.tew.infrastructure.Factories;
import com.tew.model.Usuario;

public class BeanAmigo implements Serializable {

	private static final long serialVersionUID = -1877691667714775598L;
	private List<Usuario> Amigos = null;
	private List<Usuario> invis_pendientes = null;
	private Usuario[] invitaciones = null;

	public Usuario[] getInvitaciones() {
		return invitaciones;
	}

	public void setInvitaciones(Usuario[] invitaciones) {
		this.invitaciones = invitaciones;
	}

	public List<Usuario> getInvis_pendientes() {
		return invis_pendientes;
	}

	public void setInvis_pendientes(List<Usuario> invis_pendientes) {
		this.invis_pendientes = invis_pendientes;
	}

	public String enviaInvi(String email_amigo) {

		FacesContext jsfCtx = FacesContext.getCurrentInstance();
		ResourceBundle bundle = jsfCtx.getApplication().getResourceBundle(
				jsfCtx, "msgs");
		FacesMessage msg = null;
		AmigoService service = Factories.services.createAmigoService();
		FacesContext context = FacesContext.getCurrentInstance();
		HttpServletRequest request = (HttpServletRequest) context
				.getExternalContext().getRequest();
		HttpSession httpSession = request.getSession(false);
		String email_usuario = (String) httpSession.getAttribute("Email");
		
		Amigos = service.AmigosUsuario(email_usuario);
		Iterator<Usuario> itr = Amigos.iterator();
		while(itr.hasNext()) {
			if (itr.next().getEmail().equals(email_amigo) || email_amigo.equals(email_usuario)) {
				msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						bundle.getString("inviError"), null);
				// se añade al element con id=”msg”
				FacesContext.getCurrentInstance().addMessage(null, msg);
				return "error";
			}
		}
		service.creaInvi(email_usuario, email_amigo);

		msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
				bundle.getString("inviExito"), null);
		// se añade al element con id=”msg”
		FacesContext.getCurrentInstance().addMessage(null, msg);
		return "exito";

	}
	
	public String listadoInvis() {
		AmigoService service;
		try {
			service = Factories.services.createAmigoService();
			FacesContext context = FacesContext.getCurrentInstance();
			HttpServletRequest request = (HttpServletRequest) context
					.getExternalContext().getRequest();
			HttpSession httpSession = request.getSession(false);
			String email_usuario = (String) httpSession.getAttribute("Email");
			invitaciones = (Usuario[]) service.InvisRecibidas(email_usuario)
					.toArray(new Usuario[0]);
			setInvis_pendientes(Arrays.asList(invitaciones));
			return "exito";

		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}

	}
	
	public String aceptar (Usuario u) {
		
		AmigoService service;
		try {
			service = Factories.services.createAmigoService();
			FacesContext context = FacesContext.getCurrentInstance();
			HttpServletRequest request = (HttpServletRequest) context
					.getExternalContext().getRequest();
			HttpSession httpSession = request.getSession(false);
			String email_usuario = (String) httpSession.getAttribute("Email");
			service.aceptaInvi(email_usuario, u.getEmail());
			invitaciones = (Usuario[]) service.InvisRecibidas(email_usuario)
					.toArray(new Usuario[0]);
			setInvis_pendientes(Arrays.asList(invitaciones));
			return "exito";

		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
		
	}

}
