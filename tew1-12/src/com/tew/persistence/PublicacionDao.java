package com.tew.persistence;

import java.util.List;

import com.tew.model.Publicacion;

public interface PublicacionDao {

		List<Publicacion> getPublicaciones();
		void crearPublicacion(String titulo, String texto, String email);
		List<Publicacion> getPublicacionesAmigos();
		List<Publicacion> getMisPublicaciones();

	
}
