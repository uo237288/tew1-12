package com.tew.business;

public interface ServicesFactory {

	UsuariosService createUsuariosService();
	PublicacionService createPublicacionService();
	LoginService createLoginService();
	AmigoService createAmigoService();
}
