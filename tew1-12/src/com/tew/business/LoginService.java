package com.tew.business;
import com.tew.model.Usuario;

public interface LoginService {
	
	Usuario verify(String login, String password);
}
