package com.tew.model;



public class Usuario {
	
	private static final long serialVersionUID = 6793L;
	private String email;
	private String passwd;
	private String rol;
	private String nombre;
	
	public Usuario(String correo) {
		this.email = correo;
	}
	
	public Usuario() {
	}

	public Usuario(String correo, String contra, String role, String name) {
		this.email = correo;
		this.passwd = contra;
		this.rol = role;
		this.nombre = name;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public String getRol() {
		return rol;
	}
	public void setRol(String rol) {
		this.rol = rol;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	 
	
	
}
